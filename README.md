# Why did I choose gitlab (john.eismeier@gmail.com):
- 2010 working on github to solve the distributed SCM issue
- 2010 what CI engine - try Jenkins - bye bye Cruise Control
- 2012 move CVS users to gitolite for on site solution - github fails
- 2014 deploy gitlab as experiment
- 2015 Perforce look a like gitswarm
- 2016 gitlab EE

# Gitlab CE
- ask from my PO - make gitlab self service for developers

# Perforce Gitswarm fails
- deploy gitlab EE
- need squashed commits for product migration to EE
- need help with LDAP regression
- Gitlab EE developer deliver in 2017 squashed commits !!!

# Results of gitlab CE/EE
- Projects:594  Users: 343 Groups:36 for CE
- Moving product code to EE
- Tools automation using EE